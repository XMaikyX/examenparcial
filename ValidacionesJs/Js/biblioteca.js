window.addEventListener('load',function(){
    
    nombre.addEventListener('blur',function(){
        nombrealert.innerHTML=""
        if(nombre.value ==null || nombre.value==""){
            nombrealert.innerHTML="Ingrese su nombre"
        }
    })
    cedula.addEventListener('blur',function(){
        cedalert.innerHTML=""
        if(cedula.value ==null || cedula.value==""){
            cedalert.innerHTML="Ingrese su cedula"
        }
        cedalertnum.innerHTML=""
        if(cedula.value.length!=10){
            cedalertnum.innerHTML="Ingrese 10 numero en total"
        }
        cedcorrecto.innerHTML=""
        if(!validarCed(cedula.value)){
            cedcorrecto.innerHTML="Esta cedula no es valida "
        }
    })   
    filas.addEventListener('blur',function(){
        filalert.innerHTML=""
        if(filas.value >=11 ){
            filalert.innerHTML="No sebe ser mas de 10"
        }
    })
    columnas.addEventListener('blur',function(){
        colalert.innerHTML=""
        if(columnas.value >=11){
            colalert.innerHTML="No sebe ser mas de 10"
        }
    })
})
function validarCed(numeros){
    if(numeros.length!=10)return false;
    let semaforo=false;
    resultado=0;
    numeros.substr(0,9).split('').forEach(element=>{
        let valor=semaforo ? parseInt(element): parseInt(element)*2;
        resultado+=valor>9 ? valor-9: valor;
        semaforo= !semaforo;
    })
    while(resultado >0) resultado-=10;
    return parseInt(numeros.substr(9))+ resultado==0;
}
function sololetras(letras){
    var key = letras.keyCode || letras.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}
function solonumero(numeross){
    var key = numeross.keyCode || numeross.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      numeross= "1234567890",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (numeross.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
}

